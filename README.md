# vim

#### 介绍
vim 个人IDE - 仅用于个人学习交流

#### 安装教程

```
mkdir ~/.vim
git clone https://gitee.com/zhushengle/vim
cd vim
chmod a+x vim_install.sh
./vim_install.sh
```
#### 使用说明

1. 生成检工程索文件
  在工程根目录执行：`cscope -Rkqb`
2. 使用
  随便打开一个文件，按下快捷键：`WQ` 或 `wq` ，打开折叠窗口
  按下快捷键 F4, 检索分析代码，连续点两次 Enter ，直到回到编辑界面，即可使用。
  vim 通用技巧详见：https://blog.csdn.net/StarRain2016/article/details/104740551


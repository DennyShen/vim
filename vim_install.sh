#!/bin/bash

sudo apt update
sudo apt install vim vim-scripts vim-doc exuberant-ctags cscope
vim-addons install omnicppcomplete taglist minibufexplorer winmanager supertab

tar -xvf vim-gitgutter.tgz -C ~/.vim/

tar -xvf git-nerdtree.tar.gz -C ./
cp -rf ./git-nerdtree/nerdtree_plugin ./git-nerdtree/doc ./git-nerdtree/syntax ./git-nerdtree/plugin ./git-nerdtree/lib ./git-nerdtree/autoload ~/.vim/
rm -rf git-nerdtree

tar -xvf vim-airline.tar.gz -C ./
cp -rf ./vim-airline/autoload  ./vim-airline/doc ./vim-airline/plugin ~/.vim/
rm -rf ./vim-airline

cp -rf ./plugin ./vimrc ./colors ~/.vim/

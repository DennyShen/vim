runtime! debian.vim


if has("syntax")
  syntax on
endif

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

set t_Co=256
"colorscheme ron  "delek  elflord koehler ron slate 
colorschem molokai

syntax on                                  "语法高亮
set tabstop=4                              "一个tab是4个字符
set softtabstop=4                          "按一次tab前进4个字符 
set listchars=tab:>-,trail:-
set list
autocmd FileType h,c,cpp set shiftwidth=4 | set expandtab

"设置自动缩进 
set autoindent                             "继承前一行的缩进
set nocompatible                           "不使用vi默认键盘布局
set smartindent                            "智能对齐
set cindent                                "C语言格式对齐
set ai!
set aw                                     "自动写入
"缩进的字符个数 
set cindent shiftwidth=4                   "c语言缩进4个字符
set autoindent shiftwidth=4                "智能缩进4个字符
set ruler                                  "显示状态行

set showcmd                                " 在状态栏显示目前所执行的指令，未完成的指令片段亦会显示
set showmatch                              " 高亮显示匹配的括号
set ignorecase                             " 搜索时忽略大小写
set smartcase                              " Do smart case matching
set incsearch                              " 在搜索时，输入的词句的字符高亮
"set nohlsearch                            " 不要高亮被搜索的句子
set hlsearch                               " 高亮搜索的关键字 
set autowrite                              " Automatically save before commands like :next and :make
set hidden                                 " Hide buffers when they re abandoned
set nu                                     " 设置行号 

set mouse=nv                               " 设置在哪些模式下使用鼠标功能，mouse=a 表示所有模式 
set backspace=2                            " 设置 Backspace 和 Delete 的灵活程度，backspace=2 则没有任何限制
set nowrap                                 " 不自动换行 
set textwidth=120                          " 设置超过120字符自动换行

au BufWinEnter * let w:m2=matchadd('Underlined', '\%>120v.\+', -1) " 设置超过100列的字符带下划线
syn match out80 /\%80v./ containedin=ALL 
hi out80 guifg=white guibg=red 

set cmdheight=2                            " 设置命令行的高度
set history=100                            " 设置命令历史行数
set shortmess=atI                          " 启动的时候不显示那个援助索马里儿童的提示
set novisualbell                           " 不要闪烁
set noswapfile                             " 不产生交换文件

" 括号等自动补全
:inoremap ( ()<ESC>i
:inoremap [ []<ESC>i
:inoremap { {<CR>}<ESC>i
:inoremap " ""<ESC>i
:inoremap ' ''<ESC>i
:inoremap < <><ESC>i
":inoremap / //<ESC>i
:inoremap /* /**/<ESC>i

set confirm                                " 在处理未保存或只读文件时，弹出确认
"set cursorline                            " 行高亮  
"set cursorcolumn                          " 列高亮，与函数列表有冲突

"--ctags setting--
set tags=tags
set tags+=./tags                           "表示在当前工作目录下搜索tags文件

"-- Taglist setting --
let Tlist_Ctags_Cmd='ctags'                " 因为我们放在环境变量里，所以可以直接执行
let Tlist_Use_Right_Window=1               " 让窗口显示在右边，0的话就是显示在左边
let Tlist_Show_One_File=1                  " 让taglist可以同时展示多个文件的函数列表
let Tlist_File_Fold_Auto_Close=1           " 非当前文件，函数列表折叠隐藏
let Tlist_Exit_OnlyWindow=1                " 当taglist是最后一个分割窗口时，自动推出vim
let Tlist_Process_File_Always=1            " 实时更新tags
let Tlist_Inc_Winwidth=0
let Tlist_Auto_Update=1

"-- NERDTree setting --
let g:NERDTree_title="[NERDTree]"
let g:NERDTreeShowLinesNumbers=1
let g:NERDTreeWinSize=10
let g:NERDChristmasTree=1
let g:NERDTreeShowHidden=1
let g:NERDTreeIgnore=['\.swp','\.out','tags', '\.git']
"let g:NERDTreeWinPos=1

" --NERDTree nerdtree-git-plugin setting---
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }


"-- WinManager setting --
let g:winManagerWindowLayout='NERDTree|TagList'     " 设置我们要管理的插件
let g:winManagerWidth=30                            " 设置宽度为30， 默认为25
let g:AutoOpenwinManager=0
let g:persistentBehaviour=0                         " 如果所有编辑文件都关闭了，退出vim
nmap qw :WMToggle<cr>

"-- QuickFix setting --
map <F8> :cp<CR>                                    " 按下F8，光标移到上一个错误所在的行
map <F9> :cn<CR>                                    " 按下F9，光标移到下一个错误所在的行
" 以下的映射是使上面的快捷键在插入模式下也能用
imap <F8> <ESC>:cp<CR>
imap <F9> <ESC>:cn<CR>

" -- MiniBufferExplorer --
"let g:miniBufExplMapWindowNavVim = 1               " 按下Ctrl+h/j/k/l，可以切换到当前窗口的上下左右窗口
let g:miniBufExplMapWindowNavArrows = 1             " 按下Ctrl+箭头，可以切换到当前窗口的上下左右窗口
let g:miniBufExplMapCTabSwitchBufs = 1              " 启用以下两个功能：Ctrl+tab移到下一个buffer并在当前窗口打开；Ctrl+Shift+tab移到上一个buffer并在当前窗口打开；ubuntu好像不支持
let g:miniBufExplMapCTabSwitchWindows = 1           " 启用以下两个功能：Ctrl+tab移到下一个窗口；Ctrl+Shift+tab移到上一个窗口；ubuntu好像不支持
let g:miniBufExplModSelTarget = 1                   " 不要在不可编辑内容的窗口（如TagList窗口）中打开选中的buffer
let g:miniBufExplorerMoreThanOne = 0

"-- omnicppcomplete setting --
" 以下映射语句后面不要有任何其它东西，否则会补全一些其它的东西
" imap <C-O> <C-X><C-O>
" imap <C-I> <C-X><C-I>
" imap <C-L> <C-X><C-L>
" imap <C-N> <C-X><C-N>
" imap <C-T> <C-X><C-T>
" imap <C-T> <C-X><C-K>
" imap <C-D> <C-X><C-D>

set completeopt=menu,menuone                        " 关掉智能补全时的预览窗口
let OmniCpp_MayCompleteDot = 1                      " autocomplete with .
let OmniCpp_MayCompleteArrow = 1                    " autocomplete with ->
let OmniCpp_MayCompleteScope = 1                    " autocomplete with ::
let OmniCpp_SelectFirstItem = 2                     " select first item (but don't insert)
let OmniCpp_NamespaceSearch = 2                     " search namespaces in this and included files
let OmniCpp_ShowPrototypeInAbbr = 1                 " show function prototype in popup window
let OmniCpp_GlobalScopeSearch=1                     " enable the global scope search
let OmniCpp_DisplayMode=1                           " Class scope completion mode: always show all members
"let OmniCpp_DefaultNamespaces=["std"]
let OmniCpp_ShowScopeInAbbr=1                       " show scope in abbreviation and remove the last column
let OmniCpp_ShowAccess=1

"--fold setting--
set foldmethod=syntax                               " 用语法高亮来定义折叠
set foldlevel=100                                   " 启动vim时不要自动折叠代码
set foldcolumn=5                                    " 设置折叠栏宽度

"---SuperTab----
let g:SuperTabDefaultCompletionType="context"

 """""""""""""""""""""""cscope设置""""""""""""""""""      
" set cscopequickfix=s-,c-,d-,i-,t-,e-
if has("cscope")
set csprg=/usr/bin/cscope
set csto=1
set cst
set nocsverb
" add any database in current directory
  if filereadable("cscope.out")
    cs add cscope.out
  else
    set csverb
  endif
endif

nmap <C-@>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <C-@>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <C-@>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <C-@>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <C-@>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <C-@>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <C-@>i :cs find i <C-R>=expand("<cfile>")<CR><CR>
nmap <C-@>d :cs find d <C-R>=expand("<cword>")<CR><CR>

function! UpdateCtags()                    " 更新cscope 时自动更新ctags
    if filewritable("./cscope.out")
       !ctags -R --file-scope=yes --langmap=c:+.h --languages=c,c++ --links=yes --c-kinds=+p --c++-kinds=+p --fields=+iaS --extra=+q
       TlistUpdate
       !cscope -Rkqb
       cs reset
    endif
endfunction
map <F4> :call UpdateCtags()<CR>
imap <F4> <ESC>:call UpdateCtags()<CR>

function! UpdateTlist()
    TlistUpdate
endfunction
autocmd BufWritePost *.c,*.h,*.cpp,*.S call UpdateTlist()

" ---gitgutter-setting---
let g:gitgutter_max_signs = 800
nmap ]h <Plug>GitGutterNextHunk
nmap [h <Plug>GitGutterPrevHunk

"set laststus=2
set t_Co=256
"let g:airline_powerline_fonts=1
"let g:airline_theme='bubblegum'

